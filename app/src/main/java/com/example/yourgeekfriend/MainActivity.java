package com.example.yourgeekfriend;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    static TextView answerTextView;
    static ProgressBar progressBar;
    static EditText questionEditText;

    static int progressStatus = 0; //variabile per la progress bar orizzontale durante il caricamento della descrizione

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        answerTextView = findViewById(R.id.textViewAnswer);
        questionEditText = findViewById(R.id.editTextQuestion);
        progressBar = findViewById(R.id.progressBar);

        answerTextView.setMovementMethod( new ScrollingMovementMethod()); //permette di aggiungere e utilizzare una scrolling view verticale nella text view
    }

    public void findAnswer(View v) {
        getBodyText();
    }

    private void getBodyText() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                final StringBuilder builder = new StringBuilder();
                ArrayList<String> matches = new ArrayList<>();

                searchDescriptionFrom(builder, matches);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        answerTextView.setText(builder.toString());
                    }
                });
                if(builder.length() > 0) {
                    progressStatus = 0;
                    progressBar.setProgress(progressStatus);
                }
            }
        }).start();
    }

    public void searchDescriptionFrom(StringBuilder myBuilder, ArrayList<String> myMatches) {
        try {
            Document doc = Jsoup.connect("https://www.google.com/search?q=" + questionEditText.getText().toString()).get(); //Creazione di un nuovo documneto Jsoup a partire dalla ricerca effettuata su google dell'utente in base al contenuto della editText
            Elements allLinks = doc.select(".r a");

            for (Element link : allLinks) {
                String url = link.attr("href"); //Limito la ricerca nel web sfruttando i link a wikipedia della richiesta fatta dall'utente
                if ((url.contains("https://it.wikipedia.org/wiki/")) || (url.contains("https://en.wikipedia.org/wiki/"))) {

                    //builder.append(url+"\n");
                    myMatches.add(url);
                    progressStatus += 100;
                    progressBar.setProgress(progressStatus);
                }
            }

            //builder.append(matches.get(0));

            Document wikiDoc = Jsoup.connect(myMatches.get(0)).get();
            Elements paragraphs = wikiDoc.select(".mw-content-ltr p"); //Estrazione del contenuto dei seguenti tag

            Element firstParagraph = paragraphs.first();
            Element secondParagraph = firstParagraph.nextElementSibling();
            Element thirdParagraph = secondParagraph.nextElementSibling();

            String completeDescription = firstParagraph.text() + secondParagraph.text() + thirdParagraph.text();

            completeDescription = eliminateIndex(completeDescription);

            myBuilder.append(completeDescription);

        } catch (Exception e) {
            myBuilder.append("no description found.. search again!");

            //builder.append("Error : ").append(e.getMessage()).append("\n");
        }
    }

    //QUESTA FUNZIONE VIENE UTILIZZATA PER ELIMINARE LE PARENTESI QUADRE E I NUMERETTI ALL'INTERNO DELLA DESCRIZIONE
    public String eliminateIndex(String description) {
        String parentesiAperta = "\\[";
        String parentesiChiusa = "\\]";
        String newString = description;
        for(int i=0; i<= 100; i++) {
            String indice  = parentesiAperta+i+parentesiChiusa;
            newString = newString.replaceAll(indice, "");
        }

        return newString;
    }

}
